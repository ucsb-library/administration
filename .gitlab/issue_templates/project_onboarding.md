### Onboarding Checklist

- [ ] Project Evaluation
    - [ ] The current status of the project, beyond the basic profile questions, has been documented.
    - [ ] Distance from the on-boarding [goals] (https://gitlab.com/ucsb-library/administration/issues/1#note_202215548) has been documented.
    - [ ] Frequency of modification/deployment has been identified and documented.
    - [ ] An evaluation of how hard is it to modify/deploy this project has been completed and documented.
    - [ ] Frequency that the project suffers from operational issues requiring intervention has been documented.
    - [ ] Difficulty of typical and worst-case operational interventions has been documented.
    - [ ] Risk of project data loss has been determined and documented.
    - [ ] Risk for ignificant security breaches has been determined and documented.
    - [ ] Project candidacy for content migration/sun-setting has been determined and documented.
    - [ ] Identify and open tickets for 
  - [ ] Migration Plan Created
  - [ ] Code Migrated & CI/CD Set Up
    - [ ] Version control hosting moved to GitLab;
    - [ ] CI/CD pipeline is setup in GitLab to replace any existing CI/CD pipelines.
  - [ ] Stage Migrated
  - [ ] Production Migrated

### Basic Profile
* **URL:**
* **Technology:**
* **Status:**
* **Code location:**
* **Issue tracking:**
* **Host:**
* **Authentication:**
* **Analytics:**
* **Components:**
* **Dependencies:**
* **Stakeholders:** 
* **Notes:**

